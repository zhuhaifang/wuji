#include "MEM.h"
#include "DBG.h"
#define GLOBAL_VARIABLE_DEFINE
#include "wuji.h"

WJ_Interpreter *
WJ_create_interpreter(void)
{
    MEM_Storage storage;
    WJ_Interpreter *interpreter;

    storage = MEM_open_storage(0);
    interpreter = MEM_storage_malloc(storage,
                                     sizeof(struct WJ_Interpreter_tag));
    interpreter->interpreter_storage = storage;
    interpreter->execute_storage = NULL;
    interpreter->variable = NULL;
    interpreter->function_list = NULL;
    interpreter->statement_list = NULL;
    interpreter->current_line_number = 1;
    interpreter->stack.stack_alloc_size = 0;
    interpreter->stack.stack_pointer = 0;
    interpreter->stack.stack
        = MEM_malloc(sizeof(WJ_Value) * STACK_ALLOC_SIZE);
    interpreter->heap.current_heap_size = 0;
    interpreter->heap.current_threshold = HEAP_THRESHOLD_SIZE;
    interpreter->heap.header = NULL;
    interpreter->top_environment = NULL;

    wj_set_current_interpreter(interpreter);
    add_native_functions(interpreter);

    return interpreter;
}

void
WJ_compile(WJ_Interpreter *interpreter, FILE *fp)
{
    extern int yyparse(void);
    extern FILE *yyin;

    wj_set_current_interpreter(interpreter);

    yyin = fp;
    if (yyparse()) {
        /* BUGBUG */
        fprintf(stderr, "Error ! Error ! Error !\n");
        exit(1);
    }
    wj_reset_string_literal_buffer();
}

void
WJ_interpret(WJ_Interpreter *interpreter)
{
    interpreter->execute_storage = MEM_open_storage(0);
    wj_add_std_fp(interpreter);
    wj_execute_statement_list(interpreter, NULL, interpreter->statement_list);
    wj_garbage_collect(interpreter);
}

static void
release_global_strings(WJ_Interpreter *interpreter) {
    while (interpreter->variable) {
        Variable *temp = interpreter->variable;
        interpreter->variable = temp->next;
    }
}

void
WJ_dispose_interpreter(WJ_Interpreter *interpreter)
{
    release_global_strings(interpreter);

    if (interpreter->execute_storage) {
        MEM_dispose_storage(interpreter->execute_storage);
    }
    interpreter->variable = NULL;
    wj_garbage_collect(interpreter);
    DBG_assert(interpreter->heap.current_heap_size == 0,
               ("%d bytes leaked.\n", interpreter->heap.current_heap_size));
    MEM_free(interpreter->stack.stack);
    MEM_dispose_storage(interpreter->interpreter_storage);
}

FunctionDefinition *
WJ_add_native_function(WJ_Interpreter *interpreter,
                        char *name, WJ_NativeFunctionProc *proc)
{
    FunctionDefinition *fd;

    fd = wj_malloc(sizeof(FunctionDefinition));
    fd->name = name;
    fd->type = NATIVE_FUNCTION_DEFINITION;
	fd->is_closure = WJ_FALSE;
    fd->u.native_f.proc = proc;
    fd->next = interpreter->function_list;

    interpreter->function_list = fd;

	return fd;
}
