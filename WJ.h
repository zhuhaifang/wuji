#ifndef PUBLIC_WJ_H_INCLUDED
#define PUBLIC_WJ_H_INCLUDED
#include <stdio.h>

typedef struct WJ_Interpreter_tag WJ_Interpreter;

typedef enum {
	FILE_INPUT_MODE = 1,
	STRING_INPUT_MODE
} WJ_InputMode;

WJ_Interpreter *WJ_create_interpreter(void);
void WJ_compile(WJ_Interpreter *interpreter, FILE *fp);
/* void WJ_compile_string(WJ_interpret *interpreter, char **lines); */
/* void WJ_set_command_line_args(WJ_Interpreter *interpreter, int argc, char **argv); */
void WJ_interpret(WJ_Interpreter *interpreter);
void WJ_dispose_interpreter(WJ_Interpreter *interpreter);

#endif /* PUBLIC_WJ_H_INCLUDED */
