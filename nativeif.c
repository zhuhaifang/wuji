#include "DBG.h"
#include "wuji.h"

char *
WJ_object_get_string(WJ_Object *obj)
{
    DBG_assert(obj->type == STRING_OBJECT,
               ("obj->type..%d\n", obj->type));
    return obj->u.string.string;
}

void *
WJ_object_get_native_pointer(WJ_Object *obj)
{
    DBG_assert(obj->type == NATIVE_POINTER_OBJECT,
               ("obj->type..%d\n", obj->type));
    return obj->u.native_pointer.pointer;
}

void
WJ_object_set_native_pointer(WJ_Object *obj, void *p)
{
    DBG_assert(obj->type == NATIVE_POINTER_OBJECT,
               ("obj->type..%d\n", obj->type));
    obj->u.native_pointer.pointer = p;
}

WJ_Value *
WJ_array_get(WJ_Interpreter *inter, WJ_LocalEnvironment *env,
              WJ_Object *obj, int index)
{
    return &obj->u.array.array[index];
}

void
WJ_array_set(WJ_Interpreter *inter, WJ_LocalEnvironment *env,
              WJ_Object *obj, int index, WJ_Value *value)
{
    DBG_assert(obj->type == ARRAY_OBJECT,
               ("obj->type..%d\n", obj->type));
    obj->u.array.array[index] = *value;
}

WJ_Value
WJ_create_closure(WJ_LocalEnvironment *env, FunctionDefinition *fd)
{
    WJ_Value ret;

    ret.type = WJ_CLOSURE_VALUE;
    ret.u.closure.function = fd;
    ret.u.closure.environment = env->variable;

    return ret;
}

void
WJ_set_function_definition(char *name, WJ_NativeFunctionProc *proc,
                            FunctionDefinition *fd)
{
    fd->name = name;
    fd->type = NATIVE_FUNCTION_DEFINITION;
    fd->is_closure = WJ_TRUE;
    fd->u.native_f.proc = proc;
}

