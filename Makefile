TARGET = wuji
CC=gcc
OBJS = \
  lex.yy.o\
  y.tab.o\
  main.o\
  interface.o\
  create.o\
  execute.o\
  eval.o\
  string.o\
  heap.o\
  util.o\
  native.o\
  nativeif.o\
  error.o\
  error_message.o\
  ./memory/mem.o\
  ./debug/dbg.o
CFLAGS = -c -g -Wall -Wswitch-enum -ansi -pedantic -DDEBUG
INCLUDES = \

$(TARGET):$(OBJS)
	cd ./memory; $(MAKE);
	cd ./debug; $(MAKE);
	$(CC) $(OBJS) -o $@ -lm
clean:
	rm -f *.o lex.yy.c y.tab.c y.tab.h *~
y.tab.h : wuji.y
	bison --yacc -dv wuji.y
y.tab.c : wuji.y
	bison --yacc -dv wuji.y
lex.yy.c : wuji.l wuji.y y.tab.h
	flex wuji.l
y.tab.o: y.tab.c wuji.h MEM.h
	$(CC) -c -g $*.c $(INCLUDES)
lex.yy.o: lex.yy.c wuji.h MEM.h
	$(CC) -c -g $*.c $(INCLUDES)
.c.o:
	$(CC) $(CFLAGS) $*.c $(INCLUDES)
./memory/mem.o:
	cd ./memory; $(MAKE);
./debug/dbg.o:
	cd ./debug; $(MAKE);
############################################################
create.o: create.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
error.o: error.c MEM.h wuji.h WJ.h WJ_dev.h
error_message.o: error_message.c wuji.h MEM.h WJ.h WJ_dev.h
eval.o: eval.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
execute.o: execute.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
heap.o: heap.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
interface.o: interface.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
main.o: main.c WJ.h MEM.h
native.o: native.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
nativeif.o: nativeif.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
string.o: string.c MEM.h wuji.h WJ.h WJ_dev.h
util.o: util.c MEM.h DBG.h wuji.h WJ.h WJ_dev.h
