#include <stdio.h>
#include "WJ.h"
#include "MEM.h"

int
main(int argc, char **argv)
{
    WJ_Interpreter     *interpreter;
    FILE *fp;

	/* 生成解释器 */
	/* interpreter = WJ_create_interpreter(); */

    if (argc != 2) {
		/* 如果执行时未传入文件路径则进入此分支 */
		/* 后续需修改成读入端口输入的一行代码 */
		fprintf(stderr, "usage:%s filename. \n", argv[0]);
		exit(1);

		/* while(1) { */
			/* WJ_compile(interpreter, stdin); */
			/* WJ_interpret(interpreter); */
		/* } */
    }

    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr, "%s not found.\n", argv[1]);
        exit(1);
    }
	
	/* 生成解释器 */
	interpreter = WJ_create_interpreter();

	/* 将FILE* 作为参数传递并生成分析树 */
    WJ_compile(interpreter, fp);

	/* 运行 */
    WJ_interpret(interpreter);

	/* 回收解释器 */
    WJ_dispose_interpreter(interpreter);

    MEM_dump_blocks(stdout);

    return 0;
}
