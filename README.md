# 中文编程语言设计

#### 介绍
通过参考前桥和弥所著《自制编程语言》一书，使用lex和bison两个工具辅助制作的中文解释语言。由于暂无该语言的应用方向，故取名“无极”，意为“不知所起，不知所往”。当然作为现代编程语言，目标当然是面向对象，自然语言化。使用C语言编写解释器，使其将来有可能方便在各平台中运行。

#### 软件架构
目前项目内容主要为解释器开发，待解释器开发完善、实现相对稳定的编程格式和规范后再进行功能模块开发，当前项目主要内容为
1.    debug：调试模块
2.    memory：内存管理模块
3.    module：各功能模块目录
4.    wuji：解释器主体

#### 安装教程

1.  参与开发者需再linux平台下自行安装gcc、lex、bison工具，可根据《自制编程语言》一书的步骤安装。win10平台可以安装win10的linux子系统，在应用商店中搜索“WSL”，安装其中的ubuntu系统即可。

#### 使用说明

1.  目前已能运行部分代码，将源码下载到linux系统后，在主目录上使用make命令进行编译，而后便可使用./wuji test.wj运行测试代码。
2.  测试代码包含主目录下的test.wj和test目录下的所有文件。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
